import React from "react";

const Carousel = (props) => {
  const { itemWidth, itemHeight, itemSideOffsets } = props.options;
  const itemStyle = {
    width: `${itemWidth}px`,
    height: `${itemHeight}px`,
    margin: `0px ${itemSideOffsets}px`,
  };
  const cWrapperStyle = {
    width: `${props.data.length * (itemWidth + 2 * itemSideOffsets)}px`,
    height: `${itemHeight}px`,
  };
  return (
    <div
      className="cWrapper"
      style={{ ...cWrapperStyle, transform: "translateX(0px)" }}
    >
      {props.data.map((item, index) => {
        return (
          <div className="card" key={index} style={itemStyle}>
            <img src={item.imgSrc} alt={item.title} />
            <div className="cardBody">
              <h2>{item.title}</h2>
              <p>{item.description}</p>
              <a href="/">Article</a>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Carousel;
